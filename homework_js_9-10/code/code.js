
const createCommentBlock = ({id,email,body,name}) => {
    const elBlock = document.createElement("div");
    elName = document.createElement("div");
    elEmail = document.createElement("div");
    elBody = document.createElement("div");
    btn = document.createElement("button");
    

    elBlock.className = "block-comment";

    elBlock.dataset.id = id;
    elName.textContent = name;
    elName.className = "name-block"
    elEmail.textContent = email;
    elEmail.className = "email-user"
    elBody.textContent = body;
    elBody.className = "text-coment"
    btn.textContent = "Редагування";
    btn.className = "button-edit"

    elBlock.append(elName, elEmail, elBody,btn);

    btn.onclick = function () {
      document.querySelector(".modal").style.display = "block";
      document.querySelector(".modal-title").innerHTML = name;
      document.querySelector(".modal-body").innerHTML = body;
      document.querySelector(".modal-body").setAttribute("contenteditable", "true");
    }
    
    return elBlock
}

 

const comments = data.map(el =>{
  return createCommentBlock(el);
})
  
window.onload = () =>{
  document.getElementById("comments").append(...comments);
  document.querySelector(".btn-secondary").onclick = function(){
    document.querySelector(".modal").style.display = "none"
  }
  document.querySelector(".btn-primary").onclick = function(){
    document.querySelector(".text-coment").innerHTML = document.querySelector(".modal-body")
    document.querySelector(".modal").style.display = "none"
  }
}

