window.addEventListener("DOMContentLoaded",()=>{
    //з'язуємо наш код з html-блоками
    const btn = document.querySelector(".keys"),
    display = document.querySelector(".display > input")
    const equal = document.getElementById("equal");
    const mrcBtn = document.getElementById("mrc");
    const mplus = document.getElementById("m+");
    const mminus = document.getElementById("m-");
    const mShow = document.querySelector(".m_show");
    const resBtn = document.getElementById("c_btn");


    btn.addEventListener("click", function (e) {
        //пошук і запис першої змінної
        if (/[0-9.]/.test(e.target.value) && calc.oper === "" && calc.value2 === ""){
            calc.value1 += e.target.value
            show(calc.value1, display)
         
      
        // шукаємо oper, як знак математичної оперції
        } else if (/^[/*+-]$/.test(e.target.value) && calc.value1 !== ""){
            calc.oper = e.target.value
            show(calc.oper, display)
        
            
        // шукаємо і записуємо value2
        } else if (/[0-9.]/.test(e.target.value) && calc.value1 !== "" && calc.oper !== ""){
            calc.value2 += e.target.value
            show(calc.value2, display)
        }

        
        // якщо є обидва значення і знак, кнопка = стає доступною (активною) і тепер на ній можна ловити клік
        if (calc.value1 !== "" && calc.oper !== "" && calc.value2 !== ""){
            equal.disabled = false;
        }

        //дивимося в консолі, чи дійсно всі значення і змінні зберігаються вірно (важливо для розуміння помилок!)
         console.log(calc.value1)
         console.log(calc.oper)
         console.log(calc.value2)
    })

    //натиск на = і вивід результату
   equal.addEventListener("click", function() {
    calc.rezalt = calculed(calc.value1, calc.value2, calc.oper);
    calc.value1 = calc.rezalt;
    show(calc.rezalt,display)
   })
  

   //натиск на м+ і запис значення в "пам'ять"
   mplus.addEventListener("click", function(){
    calc.memberBlock = display.value;
    mShow.innerText = "m+"
    mShow.style.display = "inline";
   })

   //натиск на м- і видалення значення з "пам'яті"
   mminus.addEventListener("click",function(){
    calc.memberBlock = "";
    mShow.innerText = "";
   })

   //натиск на mrc і запис збереженого значення, як першу змінну
   mrcBtn.addEventListener("click", function(){
      calc.value1 = calc.memberBlock;
      show(calc.value1, display);
   })

   // натиск на С і очищення всіх значень, окрім "пам'яті", але видалення означення використання "пам'яті"
   resBtn.addEventListener("click", function(){
    calc.value1 = "";
    calc.value2 = "";
    calc.oper = "";
    show(calc.value1,display);
    mShow.innerText = "";
   })
})

//всі активні змінні програми
const calc = {
    value1 : "",
    value2 : "",
    oper: "",
    memberBlock : "",
    rezalt: "",
}

//функція виводу на екранчик
function show (value, el) {
 el.value = value
}

//функція розрахунку (switch зробив наше життя легшим, а код коротшим)
function calculed (a,b,s){
    switch(s){
        case "+": return (a + b);
        case "-": return (a - b);
        case "*": return (a * b);
        case "/": return (a / b);
    }
}

