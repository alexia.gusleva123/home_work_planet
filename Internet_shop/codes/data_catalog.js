const dataCatalog = [
    {
        id: "1",
        foto: "../imgs/catalPage/image10.png",
        name: "Sportif's Original Short",
        size: ["34","36","40","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image15.png",
        colorVariant: ["blue","orange","broun","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "2",
        foto: "../imgs/catalPage/image12.png",
        name: "Sportif's Hatteras Short",
        size: ["30","38","40","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image16.png",
        colorVariant: ["blue","orange","broun","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "3",
        foto: "../imgs/catalPage/image13.png",
        name: "Sportif's Tidewater Short",
        size: ["30","32","36","42","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image17.png",
        colorVariant: ["blue","orange","green","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "4",
        foto: "../imgs/catalPage/image14.png",
        size: ["32","34","36","42","46"],
        name: "Sportif's Lauderdale Short",
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image18.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART"
    },
    {
        id: "5",
        foto: "../imgs/catalPage/image20.png",
        name: "Captain's Short",
        size: ["32","36","42","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image21.png",
        colorVariant: ["blue","orange","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "6",
        foto: "../imgs/catalPage/image24.png",
        name: "Galapagos Plain Short",
        size: ["32","36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image25.png",
        colorVariant: ["blue","orange","black","grey","broun","white"],
        btn: "ADD TO CART" 
    },
    {
        id: "7",
        foto: "../imgs/catalPage/image32.png",
        name: "Galapagos Pleated Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image33.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART" 
    },
    {
        id: "8",
        foto: "../imgs/catalPage/image34.png",
        name: "Sportif's Tidewater Denim Cargo Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 69.00,
        colors: "../imgs/catalPage/image35.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART" 
    },
    {
        id: "9",
        foto: "../imgs/catalPage/image36.png",
        name: "Marchal's Hatteras Short",
        size: ["32","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 40.99,
        colors: "../imgs/catalPage/image37.png",
        colorVariant: ["blue","orange","black","grey","broun","green"],
        btn: "ADD TO CART" 
    },
    {
        id: "10",
        foto: "../imgs/catalPage/image44.png",
        name: "Ecoths Ashcroft Short",
        size: ["32","38","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 41.99,
        colors: "../imgs/catalPage/image41.png",
        colorVariant: ["blue","orange","black","white","broun"],
        btn: "ADD TO CART"  
    },
    {
        id: "11",
        foto: "../imgs/catalPage/image45.png",
        name: "Ecoths Gannon Short",
        size: ["30","32","38","40","44","42"],
        response: "../imgs/Group 3.png",
        price: 42.99,
        colors: "../imgs/catalPage/image38.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART"
    },
    {
        id: "12",
        foto: "../imgs/catalPage/image32.png",
        name: "Galapagos Pleated Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image33.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART" 
    },
    {
        id: "13",
        foto: "../imgs/catalPage/image34.png",
        name: "Sportif's Tidewater Denim Cargo Short",
        size: ["36","38","42"],
        response: "../imgs/Group 3.png",
        price: 69.00,
        colors: "../imgs/catalPage/image35.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART" 
    },
    {
        id: "14",
        foto: "../imgs/catalPage/image36.png",
        name: "Marchal's Hatteras Short",
        size: ["32","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 40.99,
        colors: "../imgs/catalPage/image37.png",
        colorVariant: ["blue","orange","black","grey","broun","green"],
        btn: "ADD TO CART" 
    },
    {
        id: "15",
        foto: "../imgs/catalPage/image44.png",
        name: "Ecoths Ashcroft Short",
        size: ["32","38","40","42"],
        response: "../imgs/Group 3.png",
        price: 41.99,
        colors: "../imgs/catalPage/image41.png",
        colorVariant: ["blue","orange","black","white","broun"],
        btn: "ADD TO CART"  
    },
    {
        id: "16",
        foto: "../imgs/catalPage/image10.png",
        name: "Sportif's Original Short",
        size: ["34","36","40","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image15.png",
        colorVariant: ["blue","orange","broun","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "17",
        foto: "../imgs/catalPage/image12.png",
        name: "Sportif's Hatteras Short",
        size: ["30","38","40","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image16.png",
        colorVariant: ["blue","orange","broun","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "18",
        foto: "../imgs/catalPage/image13.png",
        name: "Sportif's Tidewater Short",
        size: ["30","32","36","42","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image17.png",
        colorVariant: ["blue","orange","green","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "19",
        foto: "../imgs/catalPage/image14.png",
        size: ["32","34","36","42","46"],
        name: "Sportif's Lauderdale Short",
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image18.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART"
    },
    {
        id: "20",
        foto: "../imgs/catalPage/image10.png",
        name: "Sportif's Original Short",
        size: ["34","36","40","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image15.png",
        colorVariant: ["blue","orange","broun","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "21",
        foto: "../imgs/catalPage/image12.png",
        name: "Sportif's Hatteras Short",
        size: ["30","38","40","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image16.png",
        colorVariant: ["blue","orange","broun","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "22",
        foto: "../imgs/catalPage/image13.png",
        name: "Sportif's Tidewater Short",
        size: ["30","32","36","42","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image17.png",
        colorVariant: ["blue","orange","green","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "23",
        foto: "../imgs/catalPage/image14.png",
        size: ["32","34","36","42","46"],
        name: "Sportif's Lauderdale Short",
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image18.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART"
    },
    {
        id: "24",
        foto: "../imgs/catalPage/image20.png",
        name: "Captain's Short",
        size: ["32","36","42","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image21.png",
        colorVariant: ["blue","orange","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "25",
        foto: "../imgs/catalPage/image24.png",
        name: "Galapagos Plain Short",
        size: ["32","36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image25.png",
        colorVariant: ["blue","orange","black","grey","broun","white"],
        btn: "ADD TO CART" 
    },
    {
        id: "26",
        foto: "../imgs/catalPage/image32.png",
        name: "Galapagos Pleated Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image33.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART" 
    },
    {
        id: "27",
        foto: "../imgs/catalPage/image34.png",
        name: "Sportif's Tidewater Denim Cargo Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 69.00,
        colors: "../imgs/catalPage/image35.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART" 
    },
    {
        id: "28",
        foto: "../imgs/catalPage/image36.png",
        name: "Marchal's Hatteras Short",
        size: ["32","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 40.99,
        colors: "../imgs/catalPage/image37.png",
        colorVariant: ["blue","orange","black","grey","broun","green"],
        btn: "ADD TO CART" 
    },
    {
        id: "29",
        foto: "../imgs/catalPage/image44.png",
        name: "Ecoths Ashcroft Short",
        size: ["32","38","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 41.99,
        colors: "../imgs/catalPage/image41.png",
        colorVariant: ["blue","orange","black","white","broun"],
        btn: "ADD TO CART"  
    },
    {
        id: "30",
        foto: "../imgs/catalPage/image45.png",
        name: "Ecoths Gannon Short",
        size: ["30","32","38","40","44","42","46"],
        response: "../imgs/Group 3.png",
        price: 42.99,
        colors: "../imgs/catalPage/image38.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART"
    },
    {
        id: "31",
        foto: "../imgs/catalPage/image32.png",
        name: "Galapagos Pleated Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image33.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART" 
    },
    {
        id: "32",
        foto: "../imgs/catalPage/image34.png",
        name: "Sportif's Tidewater Denim Cargo Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 69.00,
        colors: "../imgs/catalPage/image35.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART" 
    },
    {
        id: "33",
        foto: "../imgs/catalPage/image36.png",
        name: "Marchal's Hatteras Short",
        size: ["32","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 40.99,
        colors: "../imgs/catalPage/image37.png",
        colorVariant: ["blue","orange","black","grey","broun","green"],
        btn: "ADD TO CART" 
    },
    {
        id: "34",
        foto: "../imgs/catalPage/image44.png",
        name: "Ecoths Ashcroft Short",
        size: ["32","38","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 41.99,
        colors: "../imgs/catalPage/image41.png",
        colorVariant: ["blue","orange","black","white","broun"],
        btn: "ADD TO CART"  
    },
    {
        id: "35",
        foto: "../imgs/catalPage/image10.png",
        name: "Sportif's Original Short",
        size: ["34","36","40","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image15.png",
        colorVariant: ["blue","orange","broun","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "36",
        foto: "../imgs/catalPage/image12.png",
        name: "Sportif's Hatteras Short",
        size: ["30","38","40","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image16.png",
        colorVariant: ["blue","orange","broun","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "37",
        foto: "../imgs/catalPage/image13.png",
        name: "Sportif's Tidewater Short",
        size: ["30","32","36","42","44"],
        response: "../imgs/Group 3.png",
        price: 54.99,
        colors: "../imgs/catalPage/image17.png",
        colorVariant: ["blue","orange","green","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "38",
        foto: "../imgs/catalPage/image14.png",
        size: ["32","34","36","42","46"],
        name: "Sportif's Lauderdale Short",
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image18.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART"
    },
    {
        id: "39",
        foto: "../imgs/catalPage/image20.png",
        name: "Captain's Short",
        size: ["32","36","42","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image21.png",
        colorVariant: ["blue","orange","black","grey"],
        btn: "ADD TO CART"
    },
    {
        id: "40",
        foto: "../imgs/catalPage/image24.png",
        name: "Galapagos Plain Short",
        size: ["32","36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image25.png",
        colorVariant: ["blue","orange","black","grey","broun","white"],
        btn: "ADD TO CART" 
    },
    {
        id: "41",
        foto: "../imgs/catalPage/image32.png",
        name: "Galapagos Pleated Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image33.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART" 
    },
    {
        id: "42",
        foto: "../imgs/catalPage/image34.png",
        name: "Sportif's Tidewater Denim Cargo Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 69.00,
        colors: "../imgs/catalPage/image35.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART" 
    },
    {
        id: "43",
        foto: "../imgs/catalPage/image36.png",
        name: "Marchal's Hatteras Short",
        size: ["32","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 40.99,
        colors: "../imgs/catalPage/image37.png",
        colorVariant: ["blue","orange","black","grey","broun","green"],
        btn: "ADD TO CART" 
    },
    {
        id: "44",
        foto: "../imgs/catalPage/image44.png",
        name: "Ecoths Ashcroft Short",
        size: ["32","38","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 41.99,
        colors: "../imgs/catalPage/image41.png",
        colorVariant: ["blue","orange","black","white","broun"],
        btn: "ADD TO CART"  
    },
    {
        id: "45",
        foto: "../imgs/catalPage/image45.png",
        name: "Ecoths Gannon Short",
        size: ["30","32","38","40","44","42","46"],
        response: "../imgs/Group 3.png",
        price: 42.99,
        colors: "../imgs/catalPage/image38.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART"
    },
    {
        id: "46",
        foto: "../imgs/catalPage/image32.png",
        name: "Galapagos Pleated Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 34.99,
        colors: "../imgs/catalPage/image33.png",
        colorVariant: ["blue","orange","black","grey","broun"],
        btn: "ADD TO CART" 
    },
    {
        id: "47",
        foto: "../imgs/catalPage/image34.png",
        name: "Sportif's Tidewater Denim Cargo Short",
        size: ["36","38","42","46"],
        response: "../imgs/Group 3.png",
        price: 69.00,
        colors: "../imgs/catalPage/image35.png",
        colorVariant: ["blue","black"],
        btn: "ADD TO CART" 
    },
    {
        id: "48",
        foto: "../imgs/catalPage/image36.png",
        name: "Marchal's Hatteras Short",
        size: ["32","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 40.99,
        colors: "../imgs/catalPage/image37.png",
        colorVariant: ["blue","orange","black","grey","broun","green"],
        btn: "ADD TO CART" 
    },
    {
        id: "49",
        foto: "../imgs/catalPage/image44.png",
        name: "Ecoths Ashcroft Short",
        size: ["32","38","40","42","46"],
        response: "../imgs/Group 3.png",
        price: 41.99,
        colors: "../imgs/catalPage/image41.png",
        colorVariant: ["blue","orange","black","white","broun"],
        btn: "ADD TO CART"  
    },
    {
        id: "50",
        foto: "../imgs/catalPage/image10.png",
        name: "Sportif's Original Short",
        size: ["34","36","40","46"],
        response: "../imgs/Group 3.png",
        price: 67.00,
        colors: "../imgs/catalPage/image15.png",
        colorVariant: ["blue","orange","broun","grey"],
        btn: "ADD TO CART"
    }
]
export default dataCatalog;