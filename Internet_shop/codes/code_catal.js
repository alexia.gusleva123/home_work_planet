import dataCatalog from "./data_catalog.js";

const optionBtn = document.getElementById("up_ar"),
    sizeVariantsShow = document.getElementById("size_variants"),
    colorsOption = document.getElementById("up_arcol"),
    colorsVar = document.getElementById("colors"),
    [...btnPages] = document.querySelectorAll(".pages"),
    inDefault = document.getElementById("first_page"),
    [...sizeVariants] = document.querySelectorAll("#size_variants > p"),
    [...colorsVariantsBtn] = document.querySelectorAll(".col_q");


document.addEventListener("DOMContentLoaded", () => {
    inDefault.click();

})


const createProdCards = ({ id,foto, name, response, price, colors, btn }) => {
    const cards = document.getElementById("cards"),
        elLink = document.createElement("a"),
        elCard = document.createElement("div"),
        elProdFoto = document.createElement("img"),
        elProdName = document.createElement("p"),
        elProdResponse = document.createElement("img"),
        elProdPrice = document.createElement("p"),
        elProdColorVar = document.createElement("img"),
        elAddBtn = document.createElement("p");

    elCard.className = "card";
    elLink.setAttribute("href",`./product.html?id=${id}`);
    elProdFoto.className = "prod_foto";
    elProdFoto.src = foto;
    elProdName.className = "prod_name";
    elProdName.textContent = name;
    elProdResponse.className = "stars";
    elProdResponse.src = response;
    elProdPrice.className = "price";
    elProdPrice.textContent = `As low as $${price}`
    elProdColorVar.className = "prod_color_var";
    elProdColorVar.src = colors;
    elAddBtn.className = "to_cart_btn";
    elAddBtn.textContent = btn;

    cards.append(elLink);
    elLink.append(elCard);
    elCard.append(elProdFoto, elProdName, elProdResponse, elProdPrice, elProdColorVar, elAddBtn)

    return cards
}


btnPages.forEach((el) => {
    el.addEventListener("click", (e) => {
        if (e.target.id == "first_page") {
            cards.textContent = "";
            productCards1();
        } else if (e.target.id == "second_page") {
            cards.textContent = "";
            productCards2();
        } else if (e.target.id == "third_page") {
            cards.textContent = "";
            productCards3();
        } else if (e.target.id == "four_page") {
            cards.textContent = "";
            productCards4();
        } else if (e.target.id == "fifth_page") {
            cards.textContent = "";
            productCards5();
        }
    })
})

function productCards1() {
    dataCatalog.map(function (el, ind) {
        if (ind >= 0 && ind <= 9) {
            return createProdCards(el);
        }
    })
}
function productCards2() {
    dataCatalog.map(function (el, ind) {
        if (ind >= 10 && ind <= 19) {
            return createProdCards(el);
        }
    })
}
function productCards3() {
    dataCatalog.map(function (el, ind) {
        if (ind >= 20 && ind <= 29) {
            return createProdCards(el);
        }
    })
}
function productCards4() {
    dataCatalog.map(function (el, ind) {
        if (ind >= 30 && ind <= 39) {
            return createProdCards(el);
        }
    })
}
function productCards5() {
    dataCatalog.map(function (el, ind) {
        if (ind >= 40) {
            return createProdCards(el);
        }
    })
}



optionBtn.addEventListener("click", (e) => {
    sizeVariantsShow.classList.toggle("vizability");
})


sizeVariants.forEach((el) => {
    el.addEventListener("click", (e) => {
        el.distablet = true;
        let target = e.target;
        cards.textContent = "";

        dataCatalog.forEach((el_data) => {
            let sizes = el_data.size;
            sizes.map((el_size) => {
                if (target.textContent === el_size) {
                    return createProdCards(el_data);
                }
            })
        })
    })
})



colorsOption.addEventListener("click", (e) => {
    colorsVar.classList.toggle("vizability");
})


colorsVariantsBtn.forEach((el) => {
    el.addEventListener("click", (e) => {
        el.distablet = true;
        let targetC = e.target;
        cards.textContent = "";

        dataCatalog.forEach((element) => {
            let color = element.colorVariant;
            color.map((el_colors) => {
                if (targetC.id == "black_color" && el_colors == "black") {
                    return createProdCards(element);
                } else if (targetC.id == "broun_color" && el_colors == "broun") {
                    return createProdCards(element);
                } else if (targetC.id == "blue_color" && el_colors == "blue") {
                    return createProdCards(element);
                } else if (targetC.id == "green_color" && el_colors == "green") {
                    return createProdCards(element);
                } else if (targetC.id == "grey_color" && el_colors == "grey") {
                    return createProdCards(element);
                } else if (targetC.id == "orange_color" && el_colors == "orange") {
                    return createProdCards(element);
                } else if (targetC.id == "white_color" && el_colors == "white") {
                    return createProdCards(element);
                }
            })
        })
    })
})

