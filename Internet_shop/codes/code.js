const bntSubs = document.getElementById("subs"),
    validate = (p, v) => p.test(v);

const addressParagraph = document.getElementById("subscribe_btn");
const patternEmail = /^[A-z_0-9.]+@[A-z-]+.[A-z]{1,4}\.?[A-z]*$/;


export const user = {
    email: "",
    size: "",
    productName: "",
}

function inputValidate(value, patern) {
    if (validate(patern, value)) {
        user.email = value.toLowerCase();
        addressParagraph.classList.add("success");
        addressParagraph.classList.remove("error");
        document.getElementById("success_mes").style.display = "block"
        document.getElementById("error_mes").style.display = "none"
    } else {
        addressParagraph.classList.remove("success");
        addressParagraph.classList.add("error");
        user.email = "";
        document.getElementById("error_mes").style.display = "block"
        document.getElementById("success_mes").style.display = "none"
    }
}

document.addEventListener("DOMContentLoaded", () => {
    try {
        bntSubs.addEventListener("click", (e) => {
            const email = document.getElementById("address");
            inputValidate(email.value, patternEmail);
            email.value = ""
        })
    } catch (error) {
        console.error(error)
    }


})


