import dataCatalog from "./data_catalog.js";
import { user } from "./code.js";

const url = new URL(location.href);


const getQueryParam = Object.fromEntries(url.searchParams);


const createProdCards = ({ foto, name, response, price, size, colors, btn }) => {
    const product = document.getElementById("product"),
        elCard = document.createElement("div"),
        elProdFoto = document.createElement("img"),
        elProdName = document.createElement("p"),
        elProdResponse = document.createElement("img"),
        elProdPrice = document.createElement("p"),
        elNameSizeBlock = document.createElement("p"),
        elSizeVariant = document.createElement("p"),
        elNameColorBlock = document.createElement("p"),
        elProdColorVar = document.createElement("img"),
        elBtnsBlock = document.createElement("div"),
        elAddBtn = document.createElement("p"),
        elWishBnt = document.createElement("p"),
        elFrontView = document.getElementById("front_view"),
        elBackView = document.getElementById("back_view"),
        elFrontViewFoto = document.createElement("img"),
        elblackViewFoto = document.createElement("img");



    elCard.className = "card";
    elProdFoto.className = "prod_foto";
    elProdFoto.src = foto;
    elProdName.className = "prod_name";
    elProdName.textContent = name;
    elProdResponse.className = "stars";
    elProdResponse.src = response;
    elProdPrice.className = "price";
    elProdPrice.textContent = `As low as $${price}`;
    elNameColorBlock.className = "name_color_block";
    elNameColorBlock.textContent = "Color: ";
    elProdColorVar.className = "prod_color_var";
    elProdColorVar.src = colors;
    elNameSizeBlock.className = "name_size_block";
    elNameSizeBlock.textContent = "Size: ";
    elBtnsBlock.className = "btns_block";
    elAddBtn.className = "to_bag_btn";
    elAddBtn.textContent = "ADD TO BAG";
    elWishBnt.className = "wish_btn";
    elWishBnt.textContent = "ADD TO WISHLIST";
    elFrontViewFoto.className = "front_view_foto";
    elFrontViewFoto.src = foto;
    elblackViewFoto.className = "back_view_foto";
    elblackViewFoto.src = foto;

    size.forEach(element => {
        const sizeV = document.createElement("span");
        sizeV.className = "size_span";
        elSizeVariant.append(sizeV);
        sizeV.textContent += element + " ";
    });


    document.getElementById("name_product").textContent = name;
    elFrontView.append(elFrontViewFoto);
    elBackView.append(elblackViewFoto);

    product.append(elCard);
    document.getElementById("photo_prod").append(elProdFoto)
    document.getElementById("prod_info").append(elProdName, elProdResponse, elProdPrice, elNameColorBlock, elProdColorVar,
        elNameSizeBlock, elSizeVariant, elBtnsBlock);

    elBtnsBlock.append(elAddBtn, elWishBnt)
    return product;
}

let product;
let selectedSize;
dataCatalog.map((el) => {
    if (parseInt(getQueryParam.id) === parseInt(el.id)) {
        product = el;
        return createProdCards(el);
    }
})

document.addEventListener("DOMContentLoaded", () => {
    const [...sizes] = document.querySelectorAll(".size_span");
    sizes.forEach((el) => {
        el.addEventListener("click", (e) => {
            if (e.target) {
                selectedSize = + e.target.textContent;
                console.log(selectedSize)
                e.target.classList.toggle("selected");
            } else {
                /* треба якось забрати фокус з не обраного розміру */
            }
        })
    })
})
document.querySelector(".to_bag_btn").addEventListener("click", () => {
    user.productName = product.name;
    user.size = selectedSize;
    console.log(user)
    const userSaved = JSON.stringify(user);
    window.localStorage.setItem("user", userSaved);
    console.log(window.localStorage.getItem("User"))
})

