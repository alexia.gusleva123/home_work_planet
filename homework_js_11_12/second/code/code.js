
const get = (id) => document.getElementById(id);

let second = get("second");
let minute = get("minute");
 let hour = get("hour");

let timeS = 0,
timeM = 0,
timeH = 0;
timeStamp = 0;

 function count() {
 
   if(timeS <=58){
      timeS++; 
      if(timeS <=9){
        get("second").innerHTML = "0" + timeS;
      } else{
        get("second").innerHTML = timeS;
      }
      
   } else if(timeS >= 58) {
      timeS = 0;
      timeM++;
      if(timeM <= 9){
        get("minute").innerHTML = "0" + timeM;
      } else{
        get("minute").innerHTML = timeM;
      }
      
   } else if (timeM >=58){
      timeM = 0;
      timeH ++;
      if(timeH <= 9){
        get("hour").innerHTML = "0" + timeH;
      } else {
        get("hour").innerHTML = timeH;
      }
      
   }

    }


window.onload = () => {
  get("start").onclick = () => {
    timeStamp = setInterval(count,1000);
    document.querySelector(".container-stopwatch").classList.remove("red","silver") 
    document.querySelector(".container-stopwatch").classList.add ("green");
  }
  
  get("stop").onclick = () => {
    clearInterval(timeStamp);
    document.querySelector(".container-stopwatch").classList.remove("green","silver")
    document.querySelector(".container-stopwatch").classList.add ("red");
  }

  get("reset").onclick = () => {
    get("second").innerHTML = "00";
    get("minute").innerHTML = "00";
    get("hour").innerHTML = "00";
    document.querySelector(".container-stopwatch").classList.remove("green","red")
    document.querySelector(".container-stopwatch").classList.add ("silver");
  }
}