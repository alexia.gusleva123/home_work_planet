/*Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я.
Виводить на консоль повідомлення "Телефонує {name}". 
Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.*/

function Phone(number,model,weight){
    this.number = number;
    this.model = model;
    this.weight = weight;
}


Phone.prototype.receiveCall = function(name){
    console.log(`Телефонує ${name}`)
}
Phone.prototype.getNumber = function(){
    console.log(`Номер: ${this.number}`)
}

const user1 = new Phone('+380960000000','huawey i7', 16);
user1.receiveCall("Lara")
user1.getNumber();

const user2 = new Phone('+380960005000','oppo 6', 20);
user2.receiveCall("Fedir");
user2.getNumber();

const user3 = new Phone('+380960008000','iphon 12', 14);
user3.receiveCall("Vasil");
user3.getNumber();