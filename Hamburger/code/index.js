import { Hamburger, stuffings } from "./class.js";
import { setTopping, checkTopping } from "./functions.js";

const [...sizes] = document.querySelectorAll(".btn_size");
const selectSize = document.getElementById("select_size");
const [...addsBnt] = document.querySelectorAll(".bnt_adds");
const doubleClickMess = document.getElementById("double_click_mess");


const orderBurger = new Hamburger();

sizes.forEach((el) => {
    el.addEventListener("click", () => {
        if (el.id === "btn_large") {
            orderBurger.size = "Великий";
            orderBurger.price = 100;
            orderBurger.cal = 40;
            selectSize.innerText = "Обрано великий бургер";
            console.log(orderBurger)
        } else {
            orderBurger.size = "Стандартний";
            orderBurger.price = 50;
            orderBurger.cal = 20;
            selectSize.innerText = "Обрано стандартний бургер";
            console.log(orderBurger)
        }
    })
})

addsBnt.forEach((elem) => {
    elem.addEventListener("click", () => {
        if (checkTopping(orderBurger)) {
            doubleClickMess.style.display = "block";
        } else if (!checkTopping(orderBurger)) {
            setTopping(elem, orderBurger)
            console.log(orderBurger)
        }

    })
})

