 /**
    * Клас, об'єкти якого описують параметри гамбургера.
    *
    * @constructor
    * @param size Розмір
    * @param stuffing Начинка
    * @throws {HamburgerException} При неправильному використанні
    */
 //функція Hamburger(size, stuffing) { ... }
   
 /* Розміри, види начинок та добавок */
 /** 
 Hamburger.SIZE_SMALL = ...
 Hamburger.SIZE_LARGE = ...
 Hamburger.STUFFING_CHEESE = ...
 Hamburger.STUFFING_SALAD = ...
 Hamburger.STUFFING_POTATO = ...
 Hamburger.TOPPING_MAYO = ...
 Hamburger.TOPPING_SPICE = ...
*/
 /**
 * Додати добавку до гамбургера. Можна додати кілька
 * Добавок, за умови, що вони різні.
 *
 * @param topping Тип добавки
 * @throws {HamburgerException} При неправильному використанні
 */
 //Hamburger.prototype.addTopping = function (topping) ...

 /**
  * Прибрати добавку, за умови, що вона раніше була
  * Додано.
  *
  * @param topping Тип добавки
  * @throws {HamburgerException} При неправильному використанні
  */
 //Hamburger.prototype.removeTopping = function (topping) ...

 /**
  * Отримати список добавок.
  *
  * @return {Array} Масив доданих добавок містить константи
  * Hamburger.TOPPING_*
  */
 //Hamburger.prototype.getToppings = function() ...

 /**
  * Дізнатися розмір гамбургера
  */
 //Hamburger.prototype.getSize = function() ...

 /**
  * Дізнатися начинку гамбургера
  */
 //Hamburger.prototype.getStuffing = function () ...

 /**
  * Дізнатись ціну гамбургера
  * @return {Number} Ціна у тугриках
  */
 //Hamburger.prototype.calculatePrice = function () ...

 /**
  * Дізнатися калорійність
  * @return {Number} Калорійність калорій
  */
 //Hamburger.prototype.calculateCalories = function() ...

 /**
  * Надає інформацію про помилку під час роботи з гамбургером.
  * Подробиці зберігаються у властивості message.
  * @constructor
  */
 //функція HamburgerException (...) { ... }