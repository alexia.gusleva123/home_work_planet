
export function Hamburger(size, stuffing, price, cal) {
    this.size = size;
    this.stuffing = [];
    this.price = price;
    this.cal = cal;

}

Hamburger.prototype.addTopping = function (obj) {
    this.price += obj.price;
    const newStuffing = this.stuffing.push(obj.name)
    this.cal += obj.cal;
    return this;
}

Hamburger.prototype.removeTopping = function (obj) {
    this.price -= obj.price;
    this.stuffing -= obj.name;
    this.cal -= obj.cal;
    return this.price, this.stuffing;
}
export const stuffings = {
    cheese: { name: "сир", price: 10, cal: 20, id: "bnt_adds_cheese" },
    salad: { name: "салат", price: 20, cal: 5, id: "bnt_adds_salad" },
    potato: { name: "картопля", price: 15, cal: 10, id: "bnt_adds_potato" }
}
