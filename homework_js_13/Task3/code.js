import city from "./uacity.js";
/* 
Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані 

*/

const dataCity = document.getElementById("data_city");

city.forEach(element => {
    const option = document.createElement("option");
    option.value = element.city;
    dataCity.appendChild(option);
})

const userName = document.getElementById("name"),
userLastName = document.querySelector("#lastName"),
tel = document.getElementById("tel"),
email = document.getElementById("mail"),
reset = document.getElementById("reset"),
send = document.getElementById("send"),
errorMessage = document.getElementById("error_message");
const forms = document.querySelectorAll("input");




function validate (patern,value){
    return patern.test(value)
}

reset.addEventListener("click", (e) =>{
    userName.value = "";
    userLastName.value = "";
    tel.value = "";
    email.value = "";
})
send.addEventListener("click",(e) =>{
    let validFlag;
    forms.forEach(element => {
        if(element.value = ""){
            errorMessage.innerText = "Ви ввели не всі данні"
            validFlag = false;
            e.preventDefault;
        } else{
            validFlag = true;
        }
    })
    if(validFlag){
        errorMessage.innerText = "Мені нема куди зберігати ваші данні, але дякую за заповлення форм"
    }
})
userName.addEventListener("change",(e) => {
    if(validate(/^[А-я-ЇїІіЄє]+$/, e.target.value)){
        e.target.before("✅")
    } else{
        e.target.before("❌")
    }
})
userLastName.addEventListener("change",(e) => {
    if(validate(/^[А-я-ЇїІіЄє]+$/, e.target.value)){
        e.target.before("✅")
    } else{
        e.target.before("❌")
    }
})
tel.addEventListener("change",(e) =>{
    console.log(e.target.value)
    if(validate(/^\+380-\d{2}-\d{2}-\d{2}-\d{3}$/, e.target.value)){
       const phoneNumber = e.target.value;
       console.log(phoneNumber)
       const operator = phoneNumber.substring(5,7);
       parseInt(operator);
       console.log(operator)
       if(operator == 39 || operator == 67 || operator == 68
        || operator == 96 || operator == 97 || operator == 98){
            document.getElementById("kyivstar").style.display = "inline";
        } else if(operator == 50 || operator == 66 || operator == 95 || operator == 99){
            document.getElementById("vodafone").style.display = "inline";
        } else if(operator == 63 || operator == 73 || operator == 93){
            document.getElementById("lifecell").style.display =  "inline";
        } else {
            e.target.after("✅");
        }
        
    } else{
        e.target.before("❌")
    }
})

email.addEventListener("change", (e) => {
    if(validate(/^[a-z0-9]+@[a-z]+.[a-z]/, e.target.value)){
        e.target.before("✅")
    } else{
        e.target.before("❌")
    }
})