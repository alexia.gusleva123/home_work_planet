/*- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється. */



window.addEventListener("load", () => {
    const price = document.getElementById("price"),
     reset = document.getElementById("reset"),
     massege = document.getElementById("massege");

    price.addEventListener("focus",(e) => {
        price.classList.add("green_bord")
        console.log(price)
        console.log(e)
    })
    price.addEventListener("change",(e) => {
        price.classList.add("green");
        reset.style.display = "inline";
      if(parseInt(price.value) > 0){
        massege.innerText = price.value; 
      } else if(parseInt(price.value) <= 0){
        price.classList.remove("green_bord","green")
        price.classList.add("red");
        massege.innerText = "Please enter correct price"
      } 
    })

    reset.addEventListener("click",(e) => {
       price.classList.remove("green","green_bord","red");
       price.value = "";
       reset.style.display = "none";
       massege.innerText = " "
    })
})
