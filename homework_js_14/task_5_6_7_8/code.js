/*
5.Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице

6.Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body

7.Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль

8.Создайте поле для ввода данных поле введения данных выведите текст под полем*/

//task_5
const moveBlock = document.getElementById("move_block");
moveBlock.style.position = "absolute";
moveBlock.addEventListener("mousemove", function (e) {
    console.log(e)
    let x = e.clientX;
    let y = e.clientY;
    moveBlock.style.left = x + "px";
    moveBlock.style.top = y + "px";
})
//task_6
const color = document.getElementById("color_picker");
color.addEventListener("change", () => {
    console.log(color.value)
    document.getElementById("color_block").style.backgroundColor = color.value;
})

//task_7
const login = document.getElementById("login");
login.addEventListener("change", () => {
    document.getElementById("show_login").innerText = login.value;
})

//task_8
const block = document.getElementById("block");
block.addEventListener("change", () => {
    document.getElementById("user_text").innerText = block.value;
})