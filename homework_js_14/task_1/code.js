/*Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и
фамилии */

class User {
    constructor(name,lastName){
        this.name = name;
        this.lastName = lastName;
    }
    
}

window.addEventListener("DOMContentLoaded",() => {
    const userName = document.getElementById("name");
    const userLastName = document.getElementById("last_name");
    const btn = document.getElementById("btn");
    const infoBlock = document.getElementById("info_block");

    

    btn.addEventListener("click", () => {
        const user1 = new User(userName.value,userLastName.value);
       infoBlock.innerText += ` Вітаю ${user1.name} ${user1.lastName} `;
    })

})


