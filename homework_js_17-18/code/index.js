import { req, show, showStar, showRate } from "./function.js"
/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 
Курс валют НБУ з датою на який день,  https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json
героїв зоряних війн, https://swapi.dev/api/people/
список справ з https://jsonplaceholder.typicode.com/ виводити які виконані які та які ні з можливістю редагування
*/
if (localStorage.history === undefined) {
    localStorage.history = JSON.stringify([])
}




document.getElementById("todo").addEventListener("click", () => {
    req("https://jsonplaceholder.typicode.com/todos")
        .then(info => show(info));
})

document.getElementById("star_wars").addEventListener("click", () => {
    req("https://swapi.dev/api/people/")
        .then(info => showStar(info.results))
})

document.getElementById("NBU_rate").addEventListener("click", () => {
    req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
    .then(info => showRate(info))
})
document.querySelector("#close")
    .addEventListener("click", e => document.querySelector(".parent").classList.remove("active"))







