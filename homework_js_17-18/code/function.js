const spans = []

async function req(url) {
    document.querySelector(".loader").classList.add("active")
    const data = await fetch(url);
    return data.json()
}

function show(data) {
    const display = document.querySelector(".display");
    const table = `
        <table>
         <thead>
          <tr>
           <th>
           №
           </th>
           <th>
           Задача
           </th>
           <th>
           Статус
           </th>
           <th>
           Редагувати
           </th>
          </tr>
         </thead>
         <tbody>
           ${data.map((obj, i) => {
        const span = document.createElement("span");
        span.addEventListener("click", () => {
            clickHendler(obj)
        })
        span.innerHTML = "&#128295;"
        spans.push(span)
        return `
               <tr>
                    <td>
                    ${obj.id}
                    </td>
                    <td>
                    ${obj.title
            }
                    </td>
                    <td>
                    ${obj.completed ? "&#9989;" : "&#10060;"}
                    </td>
                    <td data-span="${i}">
                      
                    </td>
                </tr>`
    }).join("")}
         </tbody>
         </tablr>
        `

    display.innerHTML = "";
    display.insertAdjacentHTML("beforeend", table)
    document.querySelectorAll("td[data-span]")
        .forEach((e, i) => {
            console.log()
            e.append(spans[i])
        })
    document.querySelector(".loader").classList.remove("active")
}

function showRate(data_rate){
    const display = document.getElementById("display");
    const table =
    `
    <table>
     <thead>
      <tr>
       <th>
       Назва валюти
       </th>
       <th>
       Курс валюти
       </th>
       <th>
       Дата валідності курсу
       </th>
      </tr>
     </thead>
     <tbody>
       ${data_rate.map((obj) => {
        console.log(obj);
        return `
              <tr>
                 <td>
                  ${obj.txt}
                 </td>
                <td>
                   ${obj.rate}
                 </td>
                 <td>
                   ${obj.exchangedate}
                 </td>
             </tr>`
    }).join("")}
     </tbody >
     </tablr > `
display.innerHTML = "";
display.insertAdjacentHTML("beforeend", table);
document.querySelector(".loader").classList.remove("active")
}

function showStar(data_star) {
    const display = document.getElementById("display");
    const table =
        `
        <table>
         <thead>
          <tr>
           <th>
           Ім'я
           </th>
           <th>
           Колір волосся
           </th>
           <th>
           Зріст
           </th>
           <th>
           Вага
           </th>
           <th>
           Колір шкіри
           </th>
           <th>
           День народження
           </th>
          </tr>
         </thead>
         <tbody>
           ${data_star.map((obj) => {
            console.log(obj);
            return `
                  <tr>
                     <td>
                      ${obj.name}
                     </td>
                    <td>
                       ${obj.hair_color}
                     </td>
                     <td>
                       ${obj.height}
                     </td>
                     <td>
                       ${obj.mass}
                     </td>
                     <td>
                     ${obj.skin_color}
                   </td>
                   <td>
                   ${obj.birth_year}
                 </td>
                   
                 </tr>`
        }).join("")}
         </tbody >
         </tablr > `
    display.innerHTML = "";
    display.insertAdjacentHTML("beforeend", table);
    document.querySelector(".loader").classList.remove("active")
}


function clickHendler(obj) {
    document.querySelector(".parent").classList.add("active");
    document.getElementById("description").innerText = obj.title;
    document.getElementById("status").checked = obj.completed;

    document.getElementById("save").onclick = () => {
        console.log("+")
        const l = JSON.parse(localStorage.history);
        l.push(obj);
        localStorage.history = JSON.stringify(l)


        document.querySelector("#save")
            .addEventListener("click", e => document.querySelector(".parent").classList.remove("active"))
    }
}

export { req, show, showStar, showRate };