const touchBtn = document.getElementById("touch_btn"),
    blueBlock = document.getElementById("blue_block"),
    tapBtn = document.getElementById("tap_btn"),
    [...reset] = document.querySelectorAll(".reset"),
    moduleWindow = document.getElementById("module_questions"),
    contactsWindow = document.getElementById("contacts"),
    loginBtn = document.getElementById("login_btn"),
    searchBtn = document.getElementById("search_btn"),
    textContent = document.getElementById("text_content"),
    body = document.getElementById("body")

touchBtn.addEventListener("click", () => {
    blueBlock.style.display = "none";
})
tapBtn.addEventListener("click", () => {
    contactsWindow.style.display = "block";
    blueBlock.style.display = "block";
    body.classList.add("shadow");
})
console.log(loginBtn)
reset.forEach((el) => {
    el.addEventListener("click", () => {
        moduleWindow.style.display = "none";
        contactsWindow.style.display = "none"
        body.classList.remove("shadow")
    })
})
loginBtn.addEventListener("click",() => {
    moduleWindow.style.display = "block";
    textContent.textContent = "Seriously? For what?";
    body.classList.add("shadow");
})
searchBtn.addEventListener("click",() => {
    moduleWindow.style.display = "block";
    textContent.textContent = "What do you want to find here besides me?";
    body.classList.add("shadow");
})