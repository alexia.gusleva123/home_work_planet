const date = `© 2002 – ${new Date().getFullYear()} Мережа піцерій Domino!`,
    validate = (r, v) => r.test(v);

const user = {
    name: "",
    phone: "",
    email: ""
}
//інфо про піццу на данний момент
let pizza = {
    size: { size: "bid", price: 30 },
    sauses: [],
    tppings: ''

}
//ціноутворення піци (масив бази данних назв та цін)
const pizzaPrice = {
    sizing: [
        { size: "small", price: 10 },
        { size: "mid", price: 20 },
        { size: "bid", price: 30 }
    ],
    sause: [
        { souceName: "sauceClassic", ukName: "Кетчуп", weight: 40, price: 5 },
        { souceName: "sauceBBQ", ukName: "BBQ", weight: 40, price: 5 },
        { souceName: "sauceRikotta", ukName: "Ріккота", weight: 40, price: 5 },

        { souceName: "sauceClassic", ukName: "Кетчуп", weight: 50, price: 7 },
        { souceName: "sauceBBQ", ukName: "BBQ", weight: 50, price: 7 },
        { souceName: "sauceRikotta", ukName: "Ріккота", weight: 50, price: 7 },

        { souceName: "sauceClassic", ukName: "Кетчуп", weight: 60, price: 9 },
        { souceName: "sauceBBQ", ukName: "BBQ", weight: 60, price: 9 },
        { souceName: "sauceRikotta", ukName: "Ріккота", weight: 60, price: 9 }
    ],
    toping: [
        { topingName: "chees", weight: 40, price: 5 },
        { topingName: "fetta", weight: 40, price: 5 },
        { topingName: "mozzarella", weight: 40, price: 5 },
        { topingName: "telya", weight: 40, price: 5 },
        { topingName: "tomato", weight: 40, price: 5 },
        { topingName: "mushrooms", weight: 40, price: 5 },

        { topingName: "chees", weight: 50, price: 7 },
        { topingName: "fetta", weight: 50, price: 7 },
        { topingName: "mozzarella", weight: 50, price: 7 },
        { topingName: "telya", weight: 50, price: 7 },
        { topingName: "tomato", weight: 50, price: 7 },
        { topingName: "mushrooms", weight: 50, price: 7 },

        { topingName: "chees", weight: 60, price: 9 },
        { topingName: "fetta", weight: 60, price: 9 },
        { topingName: "mozzarella", weight: 60, price: 9 },
        { topingName: "telya", weight: 60, price: 9 },
        { topingName: "tomato", weight: 60, price: 9 },
        { topingName: "mushrooms", weight: 60, price: 9 },
    ]
}
//вивід данних піци
function showIngredients(inform) {
    const divPrice = document.querySelector(`.price span`),
        divSause = document.querySelector(`.sauces span`),
        divToping = document.querySelector(`.topings span`)

    let totalPrice = inform.size.price;

    console.log(inform)
    inform.sauses.forEach(e => {
        totalPrice += e.price
    })
    divPrice.innerText = totalPrice;
    divSause.innerText = inform.sauses.ukName;
}

window.addEventListener("DOMContentLoaded", () => {
    document.getElementById("address").innerText = date;
    const form = document.getElementById("contact-form");
    const [...inputsForm] = document.querySelectorAll("#contact-form input");
    const [...pizzaSize] = document.querySelectorAll(`#pizza input[type="radio"]`);
    //формування замовлення з урахуванням розміру і вибору клієнта
    pizzaSize.forEach(e => {
        //розмір піци
        e.addEventListener('click', (e) => {
            console.log(
                pizza.size = pizzaPrice.sizing.find(el => {
                    return el.size === e.target.value;
                }))
            showIngredients(pizza);
        })

    })
    //інгрідієнти піци
    const ingridients = document.querySelector(".ingridients");
    ingridients.addEventListener("click", (e) => {
        if (!e.target.closest('img')) { return }
        //соуси в піці
        pizza.sauses.push(pizzaPrice.sause.find(el => {
            return e.target.id === el.souceName
        }))
        showIngredients(pizza)
    })

    function inputValidate() {
        debugger
        let item = this;

        if (item.type === "text" && validate(/^[А-яіїґє-]+$/i, item.value)) {
            user.name = item.value.toLowerCase();
            item.classList.add("success");
            item.classList.remove("error")
        } else if (item.type === "email" && validate(/^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/, item.value)) {
            user.email = item.value.toLowerCase();
            item.classList.add("success");
            item.classList.remove("error")
        } else if (item.type === "tel" && validate(/^\+380[0-9]{9}$/, item.value)) {
            user.phone = item.value.toLowerCase();
            item.classList.add("success");
            item.classList.remove("error")
        }
        else {
            item.classList.add("error");
            item.classList.remove("success")
        }
    }

    inputsForm.forEach((item) => {
        if (item.type === "text" || item.type === "email" || item.type === "tel") {
            item.addEventListener("change", inputValidate)
        }
    })

    form.addEventListener("submit", (e) => {
        console.log("contact-form");

        inputsForm.forEach(function (item) {

            if (item.type === "text" || item.type === "email" || item.type === "tel") {
                inputValidate.apply(item)
            }
        })
        e.preventDefault()
    })
 
})


